/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package flexgridsim;
//

//import com.opencsv.CSVWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
//

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.w3c.dom.Element;

import com.opencsv.CSVWriter;

import flexgridsim.rsa.ControlPlaneForRSA;
import flexgridsim.rsa.RSA;
import flexgridsim.util.Distribution;
import flexgridsim.util.Modulations;

/**
 * The Control Plane is responsible for managing resources and connection within
 * the network.
 */
public class ControlPlane implements ControlPlaneForRSA {

	private RSA rsa;
	private PhysicalTopology pt;
	private VirtualTopology vt;
	private Map<Flow, ArrayList<LightPath>> mappedFlows; // Flows and lightpath that have been accepted into the network
	private Map<Flow, ArrayList<LightPath>> mappedPFlows; // Flows and lightpath protection that have been accepted into
															// the network
	private Map<Long, Flow> activeFlows; // Flows that have been accepted or that are waiting for a decision

	private Map<Long, Failure> failure; // Keep id and failure of which links have failed

	private Map<Long, ArrayList<LightPath>> protection;
	private Tracer tr = Tracer.getTracerObject();
	private MyStatistics st = MyStatistics.getMyStatisticsObject();

	//
	private List<String[]> linhas;
	private String[] c = { "id", "cos", "src", "dst", "accepted", "path", "protection" };
	private String rsaModule;
	// private Random gerador;
	private Distribution dist;
	private int[] cosDifferentiationVector;
	//

	/**
	 * Creates a new ControlPlane object.
	 *
	 * @param xml            the xml
	 * @param eventScheduler the event scheduler
	 * @param rsaModule      the name of the RSA class
	 * @param pt             the network's physical topology
	 * @param vt             the network's virtual topology
	 * @param traffic        the traffic
	 * @param ff             network failure
	 */
	@SuppressWarnings("deprecation")
	public ControlPlane(Element xml, EventScheduler eventScheduler, String rsaModule, PhysicalTopology pt,
			VirtualTopology vt, TrafficGenerator traffic) {
		@SuppressWarnings("rawtypes")
		Class RSAClass;
		mappedFlows = new HashMap<Flow, ArrayList<LightPath>>();
		mappedPFlows = new HashMap<Flow, ArrayList<LightPath>>();
		activeFlows = new HashMap<Long, Flow>();
		failure = new HashMap<Long, Failure>();
		protection = new HashMap<Long, ArrayList<LightPath>>();
		this.pt = pt;
		this.vt = vt;
		this.rsaModule = rsaModule;
		// this.gerador = new Random();
		this.dist = new Distribution(1, 3);

		int COSVectorSize = (int) (Math.pow(2, (double) traffic.getCosList().size()) - 1);
		this.cosDifferentiationVector = new int[COSVectorSize];
		int auxWeight[] = new int[traffic.getCosList().size()];
		int aux;
		for (int i = 0; i < auxWeight.length; i++) {
			aux = (int) Math.pow(2, (double) i);
			auxWeight[i] = aux;
		}

		aux = 0;
		int cos = 1;
		for (int i = 0; i < auxWeight.length; i++) {
			for (int j = 0; j < auxWeight[i]; j++) {
				this.cosDifferentiationVector[aux] = cos;
				aux++;
			}
			cos++;
		}

		//
		// String[] cabecalho = {"id", "src", "dst"};
		linhas = new ArrayList<>();
		//

		try {
			RSAClass = Class.forName(rsaModule);
			rsa = (RSA) RSAClass.newInstance();
			// rsa.simulationInterface(xml, pt, vt, this, traffic);
			rsa.simulationInterface(xml, pt, vt, this, traffic, st);
		} catch (Throwable t) {
			t.printStackTrace();
		}

	}

	public MyStatistics getSt() {
		return this.st;
	}

	/**
	 * Deals with an Event from the event queue. If it is of the FlowArrivalEvent
	 * kind, adds it to the list of active flows. If it is from the
	 * FlowDepartureEvent, removes it from the list.
	 * 
	 * @param event the Event object taken from the queue
	 */

	public void newEvent(Event event) {
		if (event instanceof FlowArrivalEvent) {

			Flow flow = ((FlowArrivalEvent) event).getFlow();

			/**
			 * long id = 4161; if(flow.getID() == id) { System.out.println("Saída " + id);
			 * }/
			 **/

			newFlow(flow);
			rsa.flowArrival(flow);

			// String.valueOf(flow.getID()), String.valueOf(flow.getSource()),
			// String.valueOf(flow.getDestination()), String.valueOf(flow.isAccepeted()),
			linhas.add(new String[] { String.valueOf(flow.getID()), String.valueOf(flow.getCOS()),
					String.valueOf(flow.getSource()), String.valueOf(flow.getDestination()),
					String.valueOf(flow.isAccepeted()), flow.printLinks(), flow.printLinksp() });

			// System.out.println(flow.printLinks());

		} else if (event instanceof FlowDepartureEvent) {
			// System.out.println(((FlowDepartureEvent) event).getID());
			/**
			 * long id = 91; if(((FlowDepartureEvent) event).getFlow().getID() == id) {
			 * System.out.println("Chegada " + id); }/
			 **/
			Flow removedFlow = removeFlow(((FlowDepartureEvent) event).getID());
			if (removedFlow != null) {
				rsa.flowDeparture(removedFlow); // Useless
			}
		} else if (event instanceof FailureIn) {
			newFailure(((FailureIn) event).getFail());
			rerouteFlowFailed(((FailureIn) event).getFail());
		} else if (event instanceof FailureOut) {
			removeFailure(((FailureOut) event).getFail());
		}
	}

	/**
	 * Retrieves a Flow object from the list of active flows.
	 * 
	 * @param id the unique identifier of the Flow object
	 * @return the required Flow object
	 */
	public Flow getFlow(long id) {
		return activeFlows.get(id);
	}

	/**
	 * Adds a given active Flow object to a determined Physical Topology.
	 * 
	 * @param id        unique identifier of the Flow object
	 * @param lightpath the Path, or list of LighPath objects
	 * @return true if operation was successful, or false if a problem occurred
	 */
	public boolean acceptFlow(long id, ArrayList<LightPath> lightpath) {
		Flow flow;
		if (id < 0) {
			throw (new IllegalArgumentException());
		} else {
			/**
			 * if (lightpath.get(0).getID() == 5209) { System.out.println("Aceitou " + id);
			 * }/
			 **/
			if (!activeFlows.containsKey(id)) {
				throw (new IllegalArgumentException());
			}
			flow = activeFlows.get(id);

			if (!canAddPathToPT(flow, lightpath)) {
				return false;
			}

			if (!flowLinksActive(flow)) {
				return false;
			}

			flow.setHasProtection(false);

			for (int i = 0; i < flow.getLinks().length; i++) {
				/**/if (flow.getLinks()[i] == 31) {
					// System.out.println("ALOCANDO 31 - CP 205");
					for (Slot slot : flow.getSlotList()) {
						/**
						 * if (slot.core == 5 && slot.slot == 13) { System.out.println(flow.getID() + "
						 * ALOCOU S. B.- 208"); }/
						 **/
					}
				} /**/
				int[] vetor = flow.getLinks();
				pt.getLink(vetor[i]).insertFlow(flow);
			}

			// addPathToPT(flow, lightpath);

			mappedFlows.put(flow, lightpath);
			tr.acceptFlow(flow, lightpath);
			st.acceptFlow(flow, lightpath);
			flow.setAccepeted(true);
			return true;
		}
	}

	// pode passar null como parametro e excluir o aceita fluxo acima
	public boolean acceptFlow(long id, ArrayList<LightPath> lightpaths, ArrayList<LightPath> lightpathsProtection) {
		Flow flow;
		int[] vetor;

		if (id < 0) {
			throw (new IllegalArgumentException());
			/**
			 * } else if(activeFlows.containsKey(id) && activeFlows.get(id).getCOS() == 2) {
			 * return acceptFlow(id, lightpaths);/
			 **/
		} else {
			/**
			 * if (id == 4745) { System.out.println("Aceitou"); }/
			 **/
			/**
			 * if (lightpaths.get(0).getID() == 5209) { System.out.println("Aceitou " + id);
			 * }/
			 **/
			/**
			 * if (lightpathsProtection.get(0).getID() == 5209) {
			 * System.out.println("Protegeu " + id); }/
			 **/
			if (!activeFlows.containsKey(id)) {
				throw (new IllegalArgumentException());
			}
			flow = activeFlows.get(id);
			if (!canAddPathToPT(flow, lightpaths)) {
				System.out.println("FaLse");
				return false;
			}

			if (!flowLinksActive(flow)) {
				return false;
			}

			/**/if (!flowLinksPActive(flow)) {
				return false; // ?
			} /**/

			for (LightPath lightpathprotection : lightpathsProtection) {
				if (!protection.containsKey(lightpathprotection.getID())) {
					ArrayList<LightPath> light = new ArrayList<LightPath>();
					/*
					if (!canAddProtectionPathToPT(flow, lightpathsProtection)) {
						System.out.println("FaLse BACK");
						return false;
					}
					*/
					addProtectionPathToPT(flow, lightpathprotection);
					light.add(lightpaths.get(lightpathsProtection.indexOf(lightpathprotection)));
					// path de proteção com id x, protege um conjunto light de paths primarios
					protection.put(lightpathprotection.getID(), light);
				} else {
					protection.get(lightpathprotection.getID())
							.add(lightpaths.get(lightpathsProtection.indexOf(lightpathprotection)));
				}
			}

			flow.setHasProtection(true);

			vetor = flow.getLinks();
			for (int i = 0; i < vetor.length; i++) {
				pt.getLink(vetor[i]).insertFlow(flow);
			}

			vetor = flow.getLinksp();
			for (int i = 0; i < vetor.length; i++) {
				pt.getLink(vetor[i]).insertPFlow(flow);
			}

			// add primary path
			// addPathToPT(flow, lightpaths);
			// addProtectionPathToPT(flow, lightpathsProtection);
			mappedFlows.put(flow, lightpaths);
			mappedPFlows.put(flow, lightpathsProtection);
			/*
			 * for(LightPath lightpath:lightpathsProtection) { lightpath.setProtection(); }
			 */
			tr.acceptFlow(flow, lightpaths);
			st.acceptFlow(flow, lightpaths, lightpathsProtection);
			flow.setAccepeted(true);
			return true;
		}
	}

	/**
	 * Removes a given Flow object from the list of active flows. //correto
	 * 
	 * @param id the unique identifier of the Flow to be removed
	 * 
	 * @return the flow object
	 */

	public Flow removeFlow(long id) {
		Flow flow;
		int[] vetor;
		ArrayList<LightPath> lightpaths;
		ArrayList<LightPath> lightpathsProtection;

		if (activeFlows.containsKey(id)) { // Check by id
			flow = activeFlows.get(id); // Return flow
			/**
			 * if (id == 4745) { System.out.println("Removeu"); }/
			 **/
			if (mappedFlows.containsKey(flow)) { // Check the existence
				lightpaths = mappedFlows.get(flow); // *******************
				if (test(lightpaths)) {
					// System.out.println(flow.getID());
				}
				removePathFromPT(flow, lightpaths); // *******************
				mappedFlows.remove(flow); // Kinda obvious

				vetor = flow.getLinks();
				for (int i = 0; i < vetor.length; i++) {
					pt.getLink(vetor[i]).removeLinkFlow(flow.getID());
				}

			} else {
				return flow;
			}

			// It's about protection

			if (mappedPFlows.containsKey(flow)) {
				// System.out.println("aqui entra");
				lightpathsProtection = mappedPFlows.get(flow);
				for (LightPath lightpathprotection : lightpathsProtection) {
					// remove lightpath do fluxo
					protection.get(lightpathprotection.getID())
							.remove(lightpaths.get(lightpathsProtection.indexOf(lightpathprotection)));
					// remove array de lightpath se estiver vazio
					if (protection.get(lightpathprotection.getID()).isEmpty()) {
						// If the lightpath to be not protecting other flow remove it
						removeProtectionPathFromPT(flow, lightpathprotection);
					}
				}
				mappedPFlows.remove(flow);
				// System.out.println(mappedPFlows.containsKey(flow));

				vetor = flow.getLinksp();
				for (int i = 0; i < vetor.length; i++) {
					pt.getLink(vetor[i]).removeLinkpFlow(flow.getID());
				}
			}
			activeFlows.remove(id);
			return flow;
		}
		return null;
	}

	private boolean test(ArrayList<LightPath> lightpaths) {
		for (LightPath lightpath : lightpaths) {
			for (Slot slot : lightpath.getSlotList()) {
				if (slot.core < 0 || slot.slot < 0 || slot.core >= pt.getNumCores() || slot.slot >= pt.getNumSlots()) {
					return true;
				}
			}
		}
		return false;
	}

	// Remove pelo id do fluxo
	/**/public Flow removeFlowProtection(long id) {
		Flow flow;
		ArrayList<LightPath> lightpaths;
		ArrayList<LightPath> lightpathsProtection;
		int[] vetor = { -1 };
		String linksp = "";

		if (activeFlows.containsKey(id)) { // Check by id
			flow = activeFlows.get(id); // Return flow
			if (mappedFlows.containsKey(flow)) { // Check the existence
				lightpaths = mappedFlows.get(flow);
				if (mappedPFlows.containsKey(flow)) {
					lightpathsProtection = mappedPFlows.get(flow);
					for (LightPath lightpathprotection : lightpathsProtection) {
						// remove lightpath do fluxo
						protection.get(lightpathprotection.getID())
								.remove(lightpaths.get(lightpathsProtection.indexOf(lightpathprotection)));
						// remove array de lightpath se estiver vazio
						if (protection.get(lightpathprotection.getID()).isEmpty()) {
							// If the lightpath to be not protecting other flow remove it
							removeProtectionPathFromPT(flow, lightpathprotection);
						}
					}

					vetor = flow.getLinksp();
					linksp = flow.printLinksp();
					for (int i = 0; i < vetor.length; i++) {
						pt.getLink(vetor[i]).removeLinkpFlow(flow.getID());
					}
					mappedPFlows.remove(flow);
				}
			}

			flow.setHasProtection(false);

			linhas.add(new String[] { "DropP", " ", "ID: " + String.valueOf(flow.getID()), " ",
					"COS: " + String.valueOf(flow.getCOS()), " ", "LinksP: " + linksp });
			return flow;
		}
		return null;
	}/**/

	// Retorna fluxos que alocaram
	/**/public ArrayList<Flow> flowsThatAllocated(LightPath lightpath, boolean protection) {
		ArrayList<Flow> flows = new ArrayList();

		if (protection) {
			for (Flow flowKey : mappedPFlows.keySet()) {
				if (mappedPFlows.get(flowKey).contains(lightpath)) {
					flows.add(flowKey);
				}
			}
		} else {
			for (Flow flowKey : mappedFlows.keySet()) {
				if (mappedFlows.get(flowKey).contains(lightpath)) {
					flows.add(flowKey);
				}
			}
		}

		return flows;
	}/**/

	// Remove pelo lightpath
	/**
	 * public Flow removeFlowProtection(LightPath lightpath) { ArrayList<Flow> flows
	 * = new ArrayList(); int[] vetor = {-1}; String linksp = "";
	 * 
	 * //ENTRAR COM UMA LISTA NO PARÂMETRO
	 * 
	 * for (Flow flowKey: mappedFlows.keySet()) {
	 * if(mappedFlows.get(flowKey).contains(lightpath)) { flows.add(flowKey); } }
	 * 
	 * if (activeFlows.containsKey(id)) { // Check by id flow = activeFlows.get(id);
	 * // Return flow if (mappedFlows.containsKey(flow)) { // Check the existence
	 * lightpaths = mappedFlows.get(flow); if (mappedPFlows.containsKey(flow)) {
	 * lightpathsProtection = mappedPFlows.get(flow); for (LightPath
	 * lightpathprotection: lightpathsProtection) { //remove lightpath do fluxo
	 * protection.get(lightpathprotection.getID()).remove(lightpaths.get(lightpathsProtection.indexOf(lightpathprotection)));
	 * //remove array de lightpath se estiver vazio
	 * if(protection.get(lightpathprotection.getID()).isEmpty()){ //If the lightpath
	 * to be not protecting other flow remove it removeProtectionPathFromPT(flow,
	 * lightpathprotection); } }
	 * 
	 * vetor = flow.getLinksp(); linksp = flow.printLinksp(); for(int i = 0; i <
	 * vetor.length; i++) { pt.getLink(vetor[i]).removeLinkpFlow(flow.getID()); }
	 * mappedPFlows.remove(flow); } }
	 * 
	 * flow.setHasProtection(false);
	 * 
	 * linhas.add(new String[] {"DropP", " ", "ID: " + String.valueOf(flow.getID()),
	 * " ", "COS: " + String.valueOf(flow.getCOS()), " ", "LinksP: " + linksp});
	 * return flow; } return null; }/
	 **/

	public Flow removeFlowProtectionToRerouting(long id) {
		Flow flow;
		// ArrayList<LightPath> lightpaths;
		// ArrayList<LightPath> lightpathsProtection;
		int[] vetor = { -1 };
		String linksp = "";

		if (activeFlows.containsKey(id)) { // Check by id
			flow = activeFlows.get(id); // Return flow
			if (mappedFlows.containsKey(flow)) { // Check the existence
				// lightpaths = mappedFlows.get(flow);
				if (mappedPFlows.containsKey(flow)) {
					// lightpathsProtection = mappedPFlows.get(flow);
					vetor = flow.getLinksp();
					linksp = flow.printLinksp();
					for (int i = 0; i < vetor.length; i++) {
						pt.getLink(vetor[i]).removeLinkpFlow(flow.getID());
					}
					mappedPFlows.remove(flow);
				}
			}
			flow.setHasProtection(false);

			linhas.add(new String[] { "DropP", " ", "ID: " + String.valueOf(flow.getID()), " ",
					"COS: " + String.valueOf(flow.getCOS()), " ", "LinksP: " + linksp });
			return flow;
		}
		return null;
	}

	public ArrayList<LightPath> getLightpathProtection(long id) {
		Flow flow;

		if (activeFlows.containsKey(id)) { // Check by id
			flow = activeFlows.get(id); // Return flow
			if (mappedFlows.containsKey(flow)) { // Check the existence
				if (mappedPFlows.containsKey(flow)) {
					return mappedPFlows.get(flow);
				}
			}
		}

		return null;
	}

	/**
	 * Removes a given Flow object from the list of active flows.
	 * 
	 * @param id unique identifier of the Flow object
	 * @return true if operation was successful, or false if a problem occurred
	 */
	public boolean blockFlow(long id) {
		Flow flow;
		if (id < 0) {
			throw (new IllegalArgumentException());
		} else {
			if (!activeFlows.containsKey(id)) {
				return false;
			}
			flow = activeFlows.get(id);
			if (mappedFlows.containsKey(flow)) {
				return false;
			}
			// System.out.println("Block " + id + " COS " + flow.getCOS());
			activeFlows.remove(id);
			tr.blockFlow(flow);
			st.blockFlow(flow);
			return true;
		}
	}

	/**
	 * Removes a given Flow object from the Physical Topology and then puts it back,
	 * but with a new route (set of LightPath objects).
	 * 
	 * @param id        unique identifier of the Flow object
	 * @param lightpath list of LightPath objects, which form a Path
	 * @return true if operation was successful, or false if a problem occurred
	 */

	public boolean rerouteFlow(long id, ArrayList<LightPath> lightpath) {
		Flow flow;
		ArrayList<LightPath> oldPath;
		if (id < 0) {
			throw (new IllegalArgumentException());
		} else {
			if (!activeFlows.containsKey(id)) {
				return false;
			}
			flow = activeFlows.get(id);
			if (!mappedFlows.containsKey(flow)) {
				return false;
			}

			for (Slot slot : flow.getSlotList()) {
				/**
				 * if (slot.core == 5 && slot.slot == 13 && slot.link == 31) {
				 * System.out.println(flow.getID() + " ALOCOU REROUTE - 522"); }/
				 **/
			}

			oldPath = mappedFlows.get(flow);
			removePathFromPT(flow, oldPath);
			if (!canAddPathToPT(flow, lightpath)) {
				addPathToPT(flow, oldPath);
				return false;
			}
			addPathToPT(flow, lightpath);
			mappedFlows.put(flow, lightpath);
			// tr.flowRequest(id, true);
			return true;
		}
	}

	public boolean backupToPrimary(long id) {
		Flow flow;
		ArrayList<LightPath> oldPath;
		ArrayList<LightPath> newPath;
		/**
		 * if(id == 6286) { System.out.println("PEGUEI"); }/
		 **/
		if (id < 0) {
			throw (new IllegalArgumentException());
		} else {
			if (!activeFlows.containsKey(id)) {
				return false;
			}
			flow = activeFlows.get(id);
			if (!mappedFlows.containsKey(flow)) {
				return false;
			}
			oldPath = mappedFlows.get(flow);
			removePathFromPT(flow, oldPath);
			// flow.clearLinks();
			// flow.clearSlotList();
			newPath = (ArrayList<LightPath>) mappedPFlows.get(flow).clone();
			removeFlowProtectionToRerouting(id);
			// flow.clearLinksp();
			// flow.clearSlotListp();
			if (!canAddPathToPT(flow, newPath)) {
				addPathToPT(flow, oldPath);
				// flow.setLinks(oldPath.get(0).getLinks());
				// flow.setSlotList(oldPath.get(0).getSlotList());
				return false;
			}
			// addPathToPT(flow, newPath);
			flow.clearLinksp();
			flow.clearSlotListp();
			flow.clearModulationLevelBack();
			flow.clearLinks();
			flow.clearSlotList();
			flow.clearModulationLevel();
			flow.setLinks(newPath.get(0).getLinks());
			flow.setSlotList(newPath.get(0).getSlotList());
			flow.setModulationLevel(newPath.get(0).getModulationLevel());

			int[] vetor = flow.getLinks();
			for (int i = 0; i < vetor.length; i++) {
				pt.getLink(vetor[i]).insertFlow(flow);
			}

			mappedFlows.put(flow, newPath);
			// tr.flowRequest(id, true);
			return true;
		}
	}

	/**
	 * Reroute flows which were using the link at the moment of the failure
	 * 
	 */

	public boolean rerouteFlowFailed(Failure failure) {

		// TODO add statistics information, check the disjoint(?), swap protection path
		// and primary path
		Map<Long, Flow> flows;
		flows = new HashMap<Long, Flow>();

		// tentar resumir tudo em > failure.getLinksFail() depois, já que a variável
		// linksfail passou
		// a fazer parte da classe-mãe Failure
		if (failure instanceof LinkFailure) {
			for (int i : ((LinkFailure) failure).getLinksFail()) {
				Map<Long, Flow> temp = new HashMap<Long, Flow>();
				temp = pt.getLink(i).getFlowsHere();
				for (Flow tt : temp.values()) {
					flows.put(tt.getID(), tt);
					// pt.getLink(i).removeLinkFlow(tt.getID());
				}
				// for(Flow tt : temp.values()) {
				// pt.getLink(i).removeLinkFlow(tt.getID());
				// }
			}
		} else if (failure instanceof NodeFailure) {
			Map<Long, Flow> temp;
			for (int i = 0; i < pt.getNumNodes(); i++) {
				temp = new HashMap<Long, Flow>();
				if (pt.hasLink(((NodeFailure) failure).getNodeFail(), i)) {
					temp = pt.getLink(((NodeFailure) failure).getNodeFail(), i).getFlowsHere();
				}
				if (pt.hasLink(i, ((NodeFailure) failure).getNodeFail())) {
					temp.putAll(pt.getLink(i, ((NodeFailure) failure).getNodeFail()).getFlowsHere());
				}
				for (Flow tt : temp.values()) {
					flows.put(tt.getID(), tt);
					// pt.getLink(i).removeLinkFlow(tt.getID());
				}
			}
		}

		for (Flow flow : flows.values()) {
			if (flow.hasProtection()) {
				if (backupToPrimary(flow.getID())) {
					// System.out.println(flow.getID() + " - COS " + flow.getCOS() + " Trocou
					// primário com proteção");
				} else {
					// System.out.println(flow.getID() + " - COS " + flow.getCOS() + " Falha ao
					// trocar primário com proteção");
				}
			} /**/else {
				removeFlow(flow.getID()); // Remove flows
				if (rsa.hasRestorationMethod()) {
					newFlow(flow);
					rsa.flowRerouting(flow);
					// System.out.printf("Reroteou COS: %d\n\n", flow.getCOS());
				}
			} /**/
		}

		return true;
	}

	/**
	 * Adds a given Flow object to the HashMap of active flows. The HashMap also
	 * stores the object's unique identifier (ID).
	 * 
	 * @param flow Flow object to be added
	 */
	private void newFlow(Flow flow) {
		activeFlows.put(flow.getID(), flow);
	}

	/**
	 * @author rafael, adriel
	 * 
	 * @param fail the one which have failed
	 * 
	 * @return void vazio
	 */

	private void newFailure(Failure ff) {
		if (ff.generateFailure()) {
			failure.put(ff.getID(), ff);
		}
	}

	// Maybe we should get info about failures and add it
	private void removeFailure(Failure ff) {
		if (failure.containsKey(ff.getID())) {
			ff.removeFailure();
			failure.remove(ff.getID());
		}
	}

	private boolean flowLinksActive(Flow flow) {
		for (int link : flow.getLinks()) {
			if (pt.getLink(link).hasFailure()) {
				this.blockFlow(flow.getID());

				ArrayList<Long> whichBlocked = pt.getLink(link).getWhichBlocked();
				for (int i = 0; i < whichBlocked.size(); i++) {
					long idfailure = whichBlocked.get(i);
					if (!failure.containsKey(idfailure)) {
						pt.getLink(link).removeWhichBlocked(idfailure);
					}
				}

				for (long id : whichBlocked) {
					failure.get(id).addFlowsFailed(flow);
				}

				return false;
			}
		}
		return true;
	}

	private boolean flowLinksPActive(Flow flow) {
		for (int link : flow.getLinksp()) {
			if (pt.getLink(link).hasFailure()) {
				this.blockFlow(flow.getID());

				ArrayList<Long> whichBlocked = pt.getLink(link).getWhichBlocked();
				for (int i = 0; i < whichBlocked.size(); i++) {
					long idfailure = whichBlocked.get(i);
					if (!failure.containsKey(idfailure)) {
						pt.getLink(link).removeWhichBlocked(idfailure);
					}
				}

				for (long id : whichBlocked) {
					failure.get(id).addFlowsFailed(flow);
				}

				return false;
			}
		}
		return true;
	}

	public int[] getLinksFail() {
		List<Integer> linklist = new ArrayList<Integer>();
		for (long id : failure.keySet()) {
			Failure flr = failure.get(id);
			linklist.addAll(flr.getLinksFail());
		}

		int[] links = new int[linklist.size()];
		for (int i = 0; i < linklist.size(); i++) {
			links[i] = linklist.get(i).intValue();
		}

		return links;
	}

	/**
	 * private boolean flowLinksPActive(Flow flow) { int[] vetor = flow.getLinksp();
	 * 
	 * for(int i = 0; i < vetor.length; i++) {
	 * if(!pt.getLink(vetor[i]).isLinkActive()) { this.blockFlow(flow.getID());
	 * 
	 * long idtemp = pt.getLink(vetor[i]).getWhichBlocked(); Failure ff =
	 * failure.get(idtemp); ff.addFlowsFailed(flow);
	 * 
	 * return false; } } return true; }/*Tem que ver isso aqui - O que fazer quando
	 * um link de proteção cai?
	 */

	/**
	 * Removes a given Flow object from a Physical Topology.
	 * 
	 * @param flow       the Flow object that will be removed from the PT
	 * @param lightpaths a list of LighPath objects
	 */

	private void removePathFromPT(Flow flow, LightPath lightpath) {
		/**
		 * for (Slot slot: lightpath.getSlotList()) {
		 * pt.getLink(slot.link).releaseSlot(slot); }/
		 **/
		vt.removeLightPath(lightpath.getID());
	}

	private void removeProtectionPathFromPT(Flow flow, LightPath lightpath) {
		/**
		 * for (Slot slot: lightpath.getSlotList()) {
		 * pt.getLink(slot.link).releaseProtectionSlot(slot,pt.getOverlap()); }/
		 **/
		vt.removeProtectionLightPath(lightpath.getID());
	}

	private void removePathFromPT(Flow flow, ArrayList<LightPath> lightpaths) {
		for (LightPath lightpath : lightpaths) {
			/**
			 * for (Slot slot: lightpath.getSlotList()) {
			 * pt.getLink(slot.link).releaseSlot(slot); }/
			 **/
			vt.removeLightPath(lightpath.getID());
		}
	}

	private void removeProtectionPathFromPT(Flow flow, ArrayList<LightPath> lightpaths) {
		for (LightPath lightpath : lightpaths) {
			/**
			 * for (Slot slot: lightpath.getSlotList()) {
			 * pt.getLink(slot.link).releaseProtectionSlot(slot,pt.getOverlap()); }/
			 **/
			vt.removeProtectionLightPath(lightpath.getID());
		}
	}

	/**
	 * Says whether or not a given Flow object can be added to a determined Physical
	 * Topology, based on the amount of bandwidth the flow requires opposed to the
	 * available bandwidth.
	 * 
	 * @param flow       the Flow object to be added
	 * @param lightpaths list of LightPath objects the flow uses
	 * @return true if Flow object can be added to the PT, or false if it can't
	 */

	private boolean canAddPathToPT(Flow flow, ArrayList<LightPath> lightpaths) {
		for (LightPath lightpath : lightpaths) {
			for (Slot slot : lightpath.getSlotList()) {
				if (pt.getLink(slot.link).isSlotsAvailable(slot)) {
					return false;
				}
			}
		}
		return true;
	}

	private boolean canAddProtectionPathToPT(Flow flow, ArrayList<LightPath> lightpaths) {
		for (LightPath lightpath : lightpaths) {
			for (Slot slot : lightpath.getSlotList()) {
				if (pt.getLink(slot.link).isProtectionSlotsAvailable(slot, pt.getOverlap())) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Adds a Flow object to a Physical Topology. This means adding the flow to the
	 * network's traffic, which simply decreases the available bandwidth.
	 * 
	 * @param flow       the Flow object to be added
	 * @param lightpaths list of LightPath objects the flow uses
	 */

	public void addPathToPT(Flow flow, LightPath lightpath) {
		for (Slot slot : lightpath.getSlotList()) {
			/**
			 * if (slot.link == 24 && slot.core == 0 && slot.slot == 35) {
			 * System.out.println(flow.getID() + "Entra 1"); }/
			 **/
			pt.getLink(slot.link).reserveSlot(slot);
		}
	}

	public void addProtectionPathToPT(Flow flow, LightPath lightpath) {
		for (Slot slot : lightpath.getSlotList()) {
			pt.getLink(slot.link).reserveProtectionSlot(slot, pt.getOverlap());
		}

	}

	public void addPathToPT(Flow flow, ArrayList<LightPath> lightpaths) {
		for (LightPath lightpath : lightpaths) {
			for (Slot slot : lightpath.getSlotList()) {
				/**
				 * if (slot.link == 24 && slot.core == 0 && slot.slot == 35) {
				 * System.out.println(flow.getID() + "Entra 2"); }/
				 **/
				pt.getLink(slot.link).reserveSlot(slot);
			}
		}
	}

	public void addProtectionPathToPT(Flow flow, ArrayList<LightPath> lightpaths) {
		for (LightPath lightpath : lightpaths) {
			for (Slot slot : lightpath.getSlotList()) {
				pt.getLink(slot.link).reserveProtectionSlot(slot, pt.getOverlap());
			}
		}
	}

	/**
	 * Retrieves a Path object, based on a given Flow object. That's possible thanks
	 * to the HashMap mappedFlows, which maps a Flow to a Path.
	 * 
	 * @param flow Flow object that will be used to find the Path object
	 * @return Path object mapped to the given flow
	 */
	public ArrayList<LightPath> getPath(Flow flow) {
		return mappedFlows.get(flow);
	}

	/**
	 * Retrieves the complete set of Flow/Path pairs listed on the mappedFlows
	 * HashMap.
	 * 
	 * @return the mappedFlows HashMap
	 */
	public Map<Flow, ArrayList<LightPath>> getMappedFlows() {
		return mappedFlows;
	}

	public int getInt(int n) {
		return dist.nextInt(n) + 1;
	}

	public int getInt() {
		int cos = cosDifferentiationVector[dist.nextInt(cosDifferentiationVector.length)];
		return cos;
	}

	/**
	 * 
	 * Return LightPaths Protected by LightPath path
	 * 
	 * @return flows
	 */
	public ArrayList<LightPath> getLightPathsProtectedByLightPath(LightPath path) {
		return protection.get(path);
	}

	/**
	 * Groom flow.
	 * 
	 * @param flow
	 *
	 * @param lp            the lp
	 * @param demandInSlots
	 */
	public void groomFlow(Flow flow, LightPath lp, int demandInSlots) {
		// boolean canGroom = true;
		// for (int linkID : lp.getLinks()) {
		// if (!pt.getLink(linkID).areSlotsAvailable(lp.getSlotList())){
		// canGroom = false;
		// break;
		// }
		// }
		// if (canGroom){
		// lp. += demandInSlots;
		// for (int linkID : lp.getLinks()) {
		// pt.getLink(linkID).reserveSlots(lp.getSlotList());
		// }
		// st.groomedFlow(flow);
		// }
	}

	/**/public Flow getFlowForSingleSide(int CoS, int links[]) {
		for (Flow flow : mappedFlows.keySet()) {
			if (flow.getCOS() == CoS && !flow.isDegraded()) {
				ArrayList<LightPath> lightpaths = mappedFlows.get(flow);
				if (lightpaths.size() > 1) {
					System.out.println("lightpath > 1");
				} else {
					LightPath lightpath = lightpaths.get(0);
					if (lightpath.getLinks().length == links.length
							&& lightpath.getDestination() == pt.getLink(links[links.length - 1]).getDestination()
							&& lightpath.getSource() == pt.getLink(links[0]).getSource()) {
						boolean test = true;
						for (int i = 0; i < lightpath.getLinks().length; i++) {
							test &= lightpath.getLink(i) == links[i];
						}
						if (test) {
							return flow;
						}
					}
				}
			}
		}
		return null;
	}/**/

	public Flow getFlowForDoubleSide(Flow flowSSD) {
		Flow bestFlow = null;
		int links[] = flowSSD.getLinks();
		for (Flow flow : mappedFlows.keySet()) {
			if (!flow.isDegraded()) {
				ArrayList<LightPath> lightpaths = mappedFlows.get(flow);
				if (lightpaths.size() > 1) {
					System.out.println("lightpath > 1");
				} else {
					LightPath lightpath = lightpaths.get(0);
					if (lightpath.getLinks().length == links.length
							&& lightpath.getDestination() == pt.getLink(links[links.length - 1]).getDestination()
							&& lightpath.getSource() == pt.getLink(links[0]).getSource()
							&& flow.getSlotList().get(0).core == flowSSD.getSlotList().get(0).core) {
						boolean test = true;
						for (int i = 0; i < lightpath.getLinks().length; i++) {
							test &= lightpath.getLink(i) == links[i];
						}
						if (bestFlow == null) {
							bestFlow = flow;
						} else if (test) {
							if ((int) Math.ceil(bestFlow.getSlotList().size() / bestFlow.getLinks().length) < (int) Math
									.ceil(flow.getSlotList().size() / flow.getLinks().length)) {
								bestFlow = flow;
							}
						}
					}
				}
			}
		}
		return bestFlow;
	}

	public void degradeFlow(Flow flow, int degradedSlots) {
		ArrayList<LightPath> lightpaths;
		int guardBand = 0;
		if (!pt.getGrooming()) {
			guardBand = 1;
		}

		if (activeFlows.containsValue(flow)) {
			if (mappedFlows.containsKey(flow)) {
				lightpaths = mappedFlows.get(flow);
				if (lightpaths.size() > 1) {
					System.out.println("LightPath > 1 CP");
				} else {
					LightPath lightpath = lightpaths.get(0);
					lightpaths = degradeInPT(lightpath, degradedSlots);

					double newRate = (lightpaths.get(0).getSlotbyLink() - guardBand)
							* (double) Modulations.getBandwidth(flow.getModulationLevel(0));// degradar
					flow.replaceSlotList(lightpaths.get(0).getSlotList());
					flow.setRate((int) newRate);
					mappedFlows.put(flow, lightpaths);
				}
			}
		}
		return;
	}

	private ArrayList<LightPath> degradeInPT(LightPath lightpath, int degradedSlots) {
		ArrayList<LightPath> lightpaths = new ArrayList<>();

		ArrayList<Slot> slotsForDegradation = new ArrayList<>();
		for (int i = lightpath.getSlotList().size() - 1; i >= lightpath.getSlotList().size()
				- degradedSlots * lightpath.getLinks().length; i--) {
			slotsForDegradation.add(lightpath.getSlotList().get(i));
		}

		ArrayList<Slot> remainingSlots = new ArrayList<>();
		for (Slot slot : lightpath.getSlotList()) {
			if (!slotsForDegradation.contains(slot)) {
				remainingSlots.add(slot);
			}
		}

		Path path = new Path(lightpath.getLinks(), remainingSlots, lightpath.getModulationLevel());
		LightPath newLightpath = new LightPath(lightpath.getID(), lightpath.getSource(), lightpath.getDestination(),
				path);

		vt.slotsDegradation(lightpath, slotsForDegradation, newLightpath);

		lightpaths.add(newLightpath);

		return lightpaths;
	}

	public void undoDegradation(Flow flow, int rate, int degradedSlots) {
		ArrayList<LightPath> lightpaths;

		if (activeFlows.containsValue(flow)) {
			if (mappedFlows.containsKey(flow)) {
				lightpaths = mappedFlows.get(flow);
				if (lightpaths.size() > 1) {
					System.out.println("LightPath > 1 CP");
				} else {
					LightPath lightpath = lightpaths.get(0);
					lightpaths = undoDegradationInPT(lightpath, degradedSlots);

					// double newRate = (lightpaths.get(0).getSlotbyLink() + degradedSlots) *
					// (double) Modulations.getBandwidth(flow.getModulationLevel());//degradar
					flow.replaceSlotList(lightpaths.get(0).getSlotList());
					flow.setRate((int) rate);
					mappedFlows.put(flow, lightpaths);
				}
			}
		}
		return;
	}

	private ArrayList<LightPath> undoDegradationInPT(LightPath lightpath, int degradedSlots) {
		ArrayList<LightPath> lightpaths = new ArrayList<>();
		ArrayList<Slot> slots = new ArrayList<>();

		int nLinkSlots = lightpath.getSlotbyLink();

		int core = lightpath.getSlotList().get(0).core;
		int lastSlot = lightpath.getSlotList().get(lightpath.getSlotList().size() - 1).slot;

		for (int i = lastSlot + 1; i <= lastSlot + degradedSlots; i++) {
			if (i >= pt.getNumSlots()) {
				System.out.print("");
			}
			for (int link : lightpath.getLinks()) {
				slots.add(new Slot(core, i, link));
			}
		}

		lightpath.getSlotList().addAll(slots);

		vt.undoSlotsDegradation(lightpath, slots);

		lightpaths.add(lightpath);

		return lightpaths;
	}

	/**/public void writeCSV(int seed, double forcedLoad, String rsaName) throws IOException {
		Writer writer = Files.newBufferedWriter(
				Paths.get(rsaName + "-flows-" + String.valueOf(forcedLoad) + "-" + String.valueOf(seed) + ".csv"));
		CSVWriter csvWriter = new CSVWriter(writer);

		csvWriter.writeNext(c);
		csvWriter.writeAll(linhas);

		csvWriter.flush();
		writer.close();
	}/**/

}
