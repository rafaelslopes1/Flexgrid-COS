/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package flexgridsim;

/**
 * Simply runs the simulation, as long as there are events
 * scheduled to happen.
 * 
 * @author andred
 */
public class SimulationRunner {

	/**
	 * Creates a new SimulationRunner object.
	 * 
	 * @param cp the the simulation's control plane
	 * @param events the simulation's event scheduler
	 */
	public SimulationRunner(){}
	public void running(ControlPlane cp, EventScheduler events) {
		Event event;
		Tracer tr = Tracer.getTracerObject();
		MyStatistics st = MyStatistics.getMyStatisticsObject();
		int a = 0;
		while ((event = events.popEvent()) != null) {
			if(!(event instanceof FailureIn || event instanceof FailureOut)) {
				tr.add(event);
				st.addEvent(event);
			}
			/**if(a==10129) {
				System.out.println(a);
			}
			a++;/**/
			cp.newEvent(event);
		}
		//System.out.println("SAIU COS 1: " + st.getArrivalsDiff(0));
	}
}