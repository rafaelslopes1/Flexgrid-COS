package flexgridsim;

public class NetworkEvent {
	protected double holdingTime;
	protected int weight;
	
	public NetworkEvent(double holdingTime, int weight) {
		this.holdingTime = holdingTime;
		this.weight = weight;
	}

	public int getWeight() {
		return weight;
	}

	public double getHoldingTime() {
		return holdingTime;
	}
	
}
