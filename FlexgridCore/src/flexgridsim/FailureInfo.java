package flexgridsim;

public class FailureInfo extends NetworkEvent {
	
	public FailureInfo(double holdingTime, int weight) {
		super(holdingTime, weight);
	}
}
