package flexgridsim;

import java.util.ArrayList;

public class FailureIn extends Event {
	//private PhysicalTopology pt;
	
	private Failure failure;
	
	public FailureIn(double time, Failure failure) {
		super(time);
		this.failure = failure;
	}
	
	public Failure getFail() {
		return failure;
	}
}
