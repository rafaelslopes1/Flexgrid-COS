package flexgridsim;

import java.util.ArrayList;

public class NodeFailure extends Failure {
	private int nodeFail;
	
	public NodeFailure(long id, PhysicalTopology pt, int node) {
		super(id, pt);
		this.nodeFail = node;
	}

	@Override
	public boolean generateFailure() {
		if(pt.hasNode(nodeFail)) {
			for(int i = 0; i < pt.getNumNodes(); i++) {
				if(pt.hasLink(nodeFail, i)) {
					pt.getLink(nodeFail, i).nodeFail();
					pt.getLink(nodeFail, i).addWhichBlocked(this.id);
					super.linksFail.add(pt.getLink(nodeFail, i).getID());
				}
				if(pt.hasLink(i, nodeFail)) {
					pt.getLink(i, nodeFail).nodeFail();
					pt.getLink(i, nodeFail).addWhichBlocked(this.id);
					super.linksFail.add(pt.getLink(i, nodeFail).getID());
				}
			}
			pt.setActiveNodeVector(nodeFail, false);
			System.out.println("Falha de nó gerada");
			return true;
		}
		return false;
	}

	@Override
	public void removeFailure() {
		for(int i = 0; i < pt.getNumNodes(); i++) {
			if(pt.hasLink(nodeFail, i)) {
				pt.getLink(nodeFail, i).nodeRepair();
			}
			if(pt.hasLink(i, nodeFail)) {
				pt.getLink(i, nodeFail).nodeRepair();
			}
			pt.setActiveNodeVector(nodeFail, true);
		}
	}
	
	public int getNodeFail() {
		return nodeFail;
	}
	
}
