package flexgridsim;

import java.util.ArrayList;

public class LinkFailure extends Failure {
	private int src, dst;
	//private ArrayList<Integer> linkFail = new ArrayList<Integer>(); // Because links are represented by a integer but it also is a pair of links in a linkVector
	
	public LinkFailure(long id, PhysicalTopology pt, int src, int dst) {
		super(id, pt);
		this.src = src;
		this.dst = dst;
		
		for(int i = 0; i < pt.getNumLinks(); i++) {
			if(pt.getLink(i).getSource() == src && pt.getLink(i).getDestination() == dst
					|| pt.getLink(i).getSource() == dst && pt.getLink(i).getDestination() == src) {
				super.linksFail.add(i); // There is a pair of links that represents only one in a topology
			}
		}
	}

	@Override
	public boolean generateFailure() {
		
		if(!pt.getLink(src, dst).isFiberCut()) {
			for(int i=0; i<super.linksFail.size(); i++) {
				pt.getLink((Integer)super.linksFail.get(i)).fiberCut(); // LinkVector
				pt.getLink((Integer)super.linksFail.get(i)).addWhichBlocked(this.id);
			}
			
			//pt.getLink(src, dst).setLinkAtivo(false);
			//pt.getLink(dst, src).setLinkAtivo(false);
			/**if(yesorno) {
				pt.removeGraph(src, dst);
				pt.removeGraph(dst, src);
			}/**/
			
			System.out.println("Falha de link gerada");
			return true;
		}
		
		return false;
	}
	
	@Override
	public void removeFailure() {
		//pt.getLink(src, dst).fiberRepair();
		//pt.getLink(dst, src).fiberRepair();
		
		for(int i=0; i<super.linksFail.size(); i++) {//fazer esse for pra reestabelecer o link
			pt.getLink(super.linksFail.get(i)).fiberRepair();
		}
		
		/**if(yesorno) {
			pt.fixGraph(src, dst);
			pt.fixGraph(dst, src);
		}/**/
		
	}
	
	/**public ArrayList<Integer> getLinkFail() {
		return linkFail;
	}/**/
}