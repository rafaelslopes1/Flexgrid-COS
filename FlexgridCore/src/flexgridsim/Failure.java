package flexgridsim;
import java.util.ArrayList;
//import flexgridsim.util.Distribution;
/**
 * @author
 * 
 */

public abstract class Failure {
	protected long id;
	protected ArrayList<Flow> flowsFail = new ArrayList<Flow>(); // Its one array per failure
	protected PhysicalTopology pt;
	//private boolean yesorno; // Think about how to include this as a option on the algorithm
	protected ArrayList<Integer> linksFail = new ArrayList<Integer>();
	
	 /**
	 * @param pt
	 * 
	 */
	
	public Failure(long id, PhysicalTopology pt) {
		this.id = id; // Identification of a Flow/Failure
		this.pt = pt;
		//this.yesorno = true;
		
	}

	public long getID() {
		return this.id;
	}

	public abstract boolean generateFailure();

	// Before I'll have already checked
	public abstract void removeFailure();
	
	
	public void addFlowsFailed(Flow flow){
		this.flowsFail.add(flow);
		//System.out.println("F");
	}
	public void removeFlowsFailed(Flow flow) {
		this.flowsFail.remove(flow);
	}
	
	
	/**
	 * It will say if I need to find another path or I just want this to be blocked
	 * */
	
	/*
	public void removeLinkGraph() {
		pt.removeGraph(src, dst);
		pt.removeGraph(dst, src);
	}
	
	public void insLinkGraph() {
		pt.fixGraph(src, dst);
		pt.fixGraph(dst, src);
	}*/
	
	public void showFlowsFailed() {
		//System.out.println("====== FLOWS-FAIL ======");
		for(int i = 0; i < this.flowsFail.size(); i++) {
			System.out.println("Falha " + i + ": " + this.flowsFail.get(i).getID() + " SRC -> DST : " + this.flowsFail.get(i).getSource() + " -> " +  this.flowsFail.get(i).getDestination());
			//System.out.println("==== LINKS DESSE FLUXO ====");
			System.out.println("N links: " + this.flowsFail.get(i).getLinks().length);
			for(int j=0; j<flowsFail.get(i).getLinks().length; j++) {
				int[] vetor = flowsFail.get(i).getLinks();
				System.out.printf("Link %d: %d\n", j, vetor[j]);
			}
		}
	}

	public ArrayList<Integer> getLinksFail() {
		//int [] links = new int[this.linksFail.size()];
		//for (int i = 0; i< this.linksFail.size(); i++) {
		//	links[i] = this.linksFail.get(i);
		//}
		//return links;
		return this.linksFail;
	}

	/*Encontro os fluxos que falharam
	public ArrayList<Flow> flowsFailed(int seed, ArrayList<Flow> flowsActive){
		ArrayList<Flow> flowsFail = new ArrayList<Flow>();
		for (Flow flow : flowsActive){
			for(int i = 0; i < flow.getLinks().length; i++){  
				if( flow.getLinks()[i]== generateFailure(seed))
					flowsFail.add(flow)	;
			}	
		}
		return flowsFail;
	}*/
}
