package flexgridsim;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVWriter;

import flexgridsim.util.Modulations;

/**
 * The Class MyStatistics.
 */
public class MyStatistics {
	private static MyStatistics singletonObject;
	private OutputManager plotter;
	private PhysicalTopology pt;
	private TrafficGenerator traffic;
	private int minNumberArrivals;
	private int numberArrivals;
	private int arrivals;
	private int departures;
	private int departedFlows;
	private int accepted;
	private int backupAcepted;
	private int blocked;
	private int requiredBandwidth;
	private int blockedBandwidth;
	private int numNodes;
	private int[][] arrivalsPairs;
	private int[][] blockedPairs;
	private int[][] requiredBandwidthPairs;
	private int[][] blockedBandwidthPairs;
	private double load;
	private double ectotal;
	private double ectrans;
	private double simTime;
	private double dataTransmitted;
	// Diff
	private int numClasses;
	private int[] arrivalsDiff;
	private int[] blockedDiff; //COS n == indice n - 1...
	private int[] requiredBandwidthDiff;
	private int[] blockedBandwidthDiff;
	private int[][][] arrivalsPairsDiff;
	private int[][][] blockedPairsDiff;
	private int[][][] requiredBandwidthPairsDiff;
	private int[][][] blockedBandwidthPairsDiff;
	private int[][] numberOfUsedTransponders;
	private int [] removedDiff;
	private int [] backupAceptedDiff;
	private int [] removedBackupDiff;
	private int [] departedFlowsDiff; 
	private int [] degradedFlowDiff;
	private String [] c;
	private int celulas;
	private ArrayList<String[]> linhas;
	private ArrayList<String[]> errosLightPath;
	private String[] infoErroLightPath;

	/**
	 * A private constructor that prevents any other class from instantiating.
	 */
	private MyStatistics() {

		numberArrivals = 0;

		arrivals = 0;
		departures = 0;
		accepted = 0;
		blocked = 0;

		requiredBandwidth = 0;
		blockedBandwidth = 0;
	}

	public int getblocked() {
		return this.blocked;
	}

	public int getBlockedDiff(int i) {
		return this.blockedDiff[i];
	}
	
	public int getRemovedDiff(int i) { //indice i ==  COS i+2
		return this.removedDiff[i];
	}
	
	public void setRemovedDiff(int i) { //indice i ==  COS i+2
		this.removedDiff[i]++;
		this.blockedDiff[i]++; //For QoP-NOODLES in GLOBECOM 2021 - RAFAEL LOPES
	}
	
	public int getRemovedBackupDiff(int i) { //indice i ==  COS i+2
		return this.removedBackupDiff[i];
	}
	
	public void setRemovedBackupDiff(int i) { //indice i ==  COS i+2
		this.removedBackupDiff[i]++;
	}
	
	public int getArrivalsDiff(int i) {
		return this.arrivalsDiff[i];
	}
	
	public  String[] getC() {
		celulas = 4 * numClasses;
		c  = new String[celulas];
		
		c[0] = "Carga";
		c[1] = "Simulação";
		c[2] = "Bloqueios";
		for(int i = 3; i<=numClasses+2; i++) {
			c[i] =  "COS [" + String.valueOf(i-2) + "]";
		}
		
		//if(numClasses != 1) {
			int k = 1;
			for (int j = numClasses +3; j <= 3*numClasses +2; j+=2) {
				c[j] =  "Removidos COS [" + String.valueOf(k) + "]"; //tá dando erro aqui
				c[j+1] =  "Proteções removidas COS [" + String.valueOf(k) + "]";
				k++;
			}
		//}
		return c;
	}
	
	public void setSlotOcupado(int link, int core, int slot, String caminhoOcupante) {
		infoErroLightPath[2] = String.valueOf(link);
		infoErroLightPath[3] = String.valueOf(core);
		infoErroLightPath[4] = String.valueOf(slot);
		infoErroLightPath[5] = caminhoOcupante;
	}
	
	public void setErroLightPath(String type, long id) {
		infoErroLightPath[0] = type;
		infoErroLightPath[1] = String.valueOf(id);
		errosLightPath.add(infoErroLightPath);		
	}
	
	public int getDegradedCoS(int i) {
		return degradedFlowDiff[i];
	}
	
	public void setDegradedCoS(int i) {
		degradedFlowDiff[i]++;
	}
	
	/**
	 * Creates a new MyStatistics object, in case it does'n exist yet.
	 * 
	 * @return the MyStatistics singletonObject
	 */
	public static synchronized MyStatistics getMyStatisticsObject() {
		if (singletonObject == null) {
			singletonObject = new MyStatistics();
		}
		return singletonObject;
	}

	/**
	 * Throws an exception to stop a cloned MyStatistics object from
	 * being created.
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

	/**
	 * Attributes initializer.
	 *
	 * @param plotter the graph plotter
	 * @param pt the pt
	 * @param traffic 
	 * @param numNodes number of nodes in the network
	 * @param numClasses number of classes of service
	 * @param minNumberArrivals minimum number of arriving events
	 * @param load the load of the network
	 */
	public void statisticsSetup(OutputManager plotter, PhysicalTopology pt, TrafficGenerator traffic, int numNodes, int numClasses, int minNumberArrivals, double load) {
		this.plotter = plotter;
		this.pt = pt;
		this.traffic = traffic;
		this.numNodes = numNodes;
		this.load =  (int)load;
		//htop
		// this.load=load/1000;
		this.arrivalsPairs = new int[numNodes][numNodes];
		this.blockedPairs = new int[numNodes][numNodes];
		this.requiredBandwidthPairs = new int[numNodes][numNodes];
		this.blockedBandwidthPairs = new int[numNodes][numNodes];

		this.minNumberArrivals = minNumberArrivals;
		numberOfUsedTransponders = new int[numNodes][numNodes];
		//Diff
		this.numClasses = numClasses;
		this.arrivalsDiff = new int[numClasses];
		this.blockedDiff = new int[numClasses];
		this.requiredBandwidthDiff = new int[numClasses];
		this.blockedBandwidthDiff = new int[numClasses];
		this.departedFlowsDiff = new int[numClasses];
		this.degradedFlowDiff = new int[numClasses];
		for (int i = 0; i < numClasses; i++) {
			this.arrivalsDiff[i] = 0;
			this.blockedDiff[i] = 0;
			this.requiredBandwidthDiff[i] = 0;
			this.blockedBandwidthDiff[i] = 0;
			this.departedFlowsDiff[i] = 0;//Increaser-QoP
			this.degradedFlowDiff[i] = 0;
		}
		this.arrivalsPairsDiff = new int[numNodes][numNodes][numClasses];
		this.blockedPairsDiff = new int[numNodes][numNodes][numClasses];
		this.requiredBandwidthPairsDiff = new int[numNodes][numNodes][numClasses];
		this.blockedBandwidthPairsDiff = new int[numNodes][numNodes][numClasses];
		this.ectotal = 0;
		this.ectrans = 0;
		this.simTime = 0;
		this.dataTransmitted = 0;
		this.removedDiff = new int[numClasses];
		this.backupAceptedDiff = new int[numClasses];
		this.removedBackupDiff = new int[numClasses];
		for (int i = 0; i < numClasses-1; i++) {
			this.removedDiff[i] = 0;
			this.removedBackupDiff[i] = 0;	
		}
		
		backupAcepted = 0;
		
		linhas = new ArrayList<>();
		
		errosLightPath = new ArrayList<>();
		infoErroLightPath = new String[6];
		
		departedFlows = 0;
		
	}
	/**
	 * Calculate last statistics for the graph generation.
	 */
	public void calculateLastStatistics(){
		//bandwidth block graph
		plotter.addDotToGraph("mbbr", load, ((float) blockedBandwidth) / ((float) requiredBandwidth));
		plotter.addDotToGraph("bp", load, ((float) blocked) / ((float) arrivals) * 100);
		int count = 0;
		float bbr, jfi, sum1 = 0, sum2 = 0;
		if (blocked == 0) {
			bbr = 0;
		} else {
			bbr = ((float) blockedBandwidth) / ((float) requiredBandwidth) * 100;
		}
		for (int i = 0; i < numNodes; i++) {
			for (int j = i + 1; j < numNodes; j++) {
				if (i != j) {
					if (blockedPairs[i][j] == 0) {
						bbr = 0;
					} else {
						bbr = ((float) blockedBandwidthPairs[i][j]) / ((float) requiredBandwidthPairs[i][j]) * 100;
					}
					count++;
					sum1 += bbr;
					sum2 += bbr * bbr;
				}
			}
		}
		jfi = (sum1 * sum1) / ((float) count * sum2);
		plotter.addDotToGraph("jfi", load, jfi);
		//POWE CONSUPTION
		double ecoxc = 0;
		for (int i = 0; i < pt.getNumNodes(); i++) {
			//OXCs consume 150 Watts and each port 85 Watts. 
			ecoxc += pt.getNodeDegree(i) * 85 + 150;
		}
		// (*tempo de operação)
		ecoxc=ecoxc*simTime;
		//EDFAs have a constant power consumption of 200 Watts. (*tempo de operação)
		double ecedfa = pt.getNumLinks() * (60*pt.getNumCores()+140) ;
		//(*tempo de operação)
		ecedfa= ecedfa*simTime;

		//converte para kj
		ectotal=  (ectrans + ecoxc + ecedfa);


		//The data transmitted is given by the product of the transmission rate of the flow (TRFlow) and the flow duration(Mbits).
		plotter.addDotToGraph("data",load, dataTransmitted);
		// Energy consumption (Joules)
		plotter.addDotToGraph("pc",load, ectotal/(simTime*1000));//tira o mil vira kjoules
		//energy efficiency  (Mb/Joule) is defined as	the ratio between the total data transmitted, and the total energy consumed in the network
		plotter.addDotToGraph("ee",load, dataTransmitted/(ectotal/1000));
		//?
		plotter.addDotToGraph("ee2",load, (((float) blockedBandwidth) / ((float) requiredBandwidth)) / (ectotal/(simTime*1000)));
		
		for(int i=0;i<numClasses; i++) {
			plotter.addDotToGraph("block"+String.valueOf(i+1), load, ((float) blockedBandwidthDiff[i] / (float) requiredBandwidth));
			plotter.addDotToGraph("backrate"+String.valueOf(i+1), load, ((float) backupAceptedDiff[i] / (float) arrivalsDiff[i]));
			plotter.addDotToGraph("acepted"+String.valueOf(i+1), load, ((float) arrivalsDiff[i] / (float) arrivals));
			plotter.addDotToGraph("removed"+String.valueOf(i+1), load, ((float) removedDiff[i] / (float) accepted));
			plotter.addDotToGraph("removedbackup"+String.valueOf(i+1), load, ((float) removedBackupDiff[i] / (float) backupAcepted));
			plotter.addDotToGraph("departed"+String.valueOf(i+1), load, ((float) departedFlowsDiff[i] / (float) accepted));
		}
		plotter.addDotToGraph("backrateall", load, ((float) backupAcepted / (float) accepted));
		plotter.addDotToGraph("departurerate" , load, ((float) departedFlows / (float) accepted));
	}

	/**
	 * Calculate periodical statistics.
	 */
	public void calculatePeriodicalStatistics(){
		//fragmentation graph
		double fragmentationMean = 0;
		for (int i = 0; i < pt.getNumLinks(); i++) {
			try {
				fragmentationMean += pt.getLink(i).getFragmentationRatio(traffic.getCallsTypeInfo(), pt.getSlotCapacity());//pt.getSlotCapacity());
			} catch (NullPointerException e) {

			}
		}

		fragmentationMean = fragmentationMean / pt.getNumLinks();
		//System.out.println("Frag:"+fragmentationMean);
		if (Double.compare(fragmentationMean, Double.NaN)!=0){
			plotter.addDotToGraph("fragmentation",load, fragmentationMean);
		}
		double meanTransponders = 0;
		for (int i = 0; i < numberOfUsedTransponders.length; i++) {
			for (int j = 0; j < numberOfUsedTransponders[i].length; j++) {
				if (numberOfUsedTransponders[i][j]>0){
					meanTransponders += numberOfUsedTransponders[i][j];
				}
			}
		}
		//    	meanTransponders = meanTransponders / size;
		if (Double.compare(meanTransponders, Double.NaN)!=0){
			plotter.addDotToGraph("transponders",load, meanTransponders);
		}


		//double xtps = 0;
		double xtpsP = 0;
		//int linksXtps = 0;
		int linksXtpsP = 0;
		//double crossIndecibeisA =0;
		//double crossIndecibeisB =0;
		//double crossIndecibeisC =0;
		//double crossIndecibeisD =0;
		for (int i = 0; i < pt.getNumLinks(); i++) {
			try {
				//crossIndecibeisA +=  pt.getLink(i).getCrossTalkDB('A');
				//crossIndecibeisB +=  pt.getLink(i).getCrossTalkDB('B');
				//crossIndecibeisC +=  pt.getLink(i).getCrossTalkDB('C');
				//crossIndecibeisD +=  pt.getLink(i).getCrossTalkDB('D');

				/* Crosstalk velho*/
				//	double xt = pt.getLink(i).getCrossTalkPerSlot();
				double xtP = pt.getLink(i).getCrossTalkPerSlotb();
				//	if (xt!=-1){
				//	xtps += xt;
				//	linksXtps++;
				//}
				if (xtP!=-1){
					xtpsP += xtP;
					linksXtpsP++;
				}
			} catch (NullPointerException e) {

			}
		}
		/* Crosstalk em Decibeis*/
		//plotter.addDotToGraph("decibeisA",load, crossIndecibeisA/ pt.getNumLinks());
		//plotter.addDotToGraph("decibeisB",load, crossIndecibeisB/ pt.getNumLinks());
		//plotter.addDotToGraph("decibeisC",load, crossIndecibeisC/ pt.getNumLinks());
		//plotter.addDotToGraph("decibeisD",load, crossIndecibeisD/ pt.getNumLinks());


		/*	if (xtps!=0)
    		plotter.addDotToGraph("xtpsok",load, xtps/ linksXtps);*/

		/* Crosstalk de Proteção*/
		if (xtpsP!=0)
			plotter.addDotToGraph("xtps",load, xtpsP/ linksXtpsP);
	}

	/**
	 * Adds an accepted flow to the statistics.
	 * 
	 * @param flow the accepted Flow object
	 * @param lightpath lightpath of the flow
	 */
	public void acceptFlow(Flow flow, ArrayList<LightPath> lightpath,  ArrayList<LightPath> lightpath2) {
		if (this.numberArrivals > this.minNumberArrivals){
			this.accepted++;    
			this.backupAcepted++;
			this.backupAceptedDiff[flow.getCOS()-1]++; 
			int links =  flow.getSizeAllLinks()+1;
			int linksp = flow.getSizeAllLinksp();
			int linkst =  links+1+ linksp;
			int slotsocuped= ( links + linksp-1)*(int)(Math.ceil(flow.getRate() / (double) pt.getSlotCapacity()));
			plotter.addDotToGraph("modulation",load, flow.getModulationLevel());
			plotter.addDotToGraph("hops",load, links);
			plotter.addDotToGraph("hopsback",load, linksp);
			plotter.addDotToGraph("slotsoccupied",load, linkst*slotsocuped);
			//taxa em bytes * duracao
			dataTransmitted += flow.getRate();
			ectrans += flow.getDuration() * (flow.getSlotList().size()-1)* Modulations.getPowerConsumption(flow.getModulationLevel());
			numberOfUsedTransponders[flow.getSource()][flow.getDestination()]++;
		}
	}
	public void acceptFlow(Flow flow, ArrayList<LightPath> lightpath) {
		if (this.numberArrivals > this.minNumberArrivals){
			this.accepted++;
			int links =  flow.getSizeAllLinks()+1;
			int linksp = flow.getSizeAllLinksp();
			int linkst =  links+1+ linksp;
			int slotsocuped= ( links-1)*(int)(Math.ceil(flow.getRate() / (double) pt.getSlotCapacity()));
			plotter.addDotToGraph("modulation",load, flow.getModulationLevel());
			plotter.addDotToGraph("hops",load, links);
			plotter.addDotToGraph("slotsoccupied",load, linkst*slotsocuped);
			//taxa em Mbs * duracao
			dataTransmitted += flow.getRate();



			ectrans += flow.getDuration() *( flow.getSlotList().size()-1) * Modulations.getPowerConsumption(flow.getModulationLevel());





			numberOfUsedTransponders[flow.getSource()][flow.getDestination()]++;
		}
	}
	/**
	 * Groomed flow.
	 *
	 * @param flow the flow
	 */
	public void groomedFlow(Flow flow){
		if (this.numberArrivals > this.minNumberArrivals){
			//taxa em bytes * duracao
			dataTransmitted += flow.getRate();
			ectrans += flow.getDuration() * (flow.getSlotList().size()-1) *Modulations.getPowerConsumption(flow.getModulationLevel());
		}
	}
	/**
	 * Adds a blocked flow to the statistics.
	 * 
	 * @param flow the blocked Flow object
	 */
	public void blockFlow(Flow flow) {
		if (this.numberArrivals > this.minNumberArrivals) {
			this.blocked++;
			int cos = flow.getCOS()-1;
			this.blockedDiff[cos]++;
			this.blockedBandwidth += flow.getRate();
			this.blockedBandwidthDiff[cos] += flow.getRate();
			this.blockedPairs[flow.getSource()][flow.getDestination()]++;
			this.blockedPairsDiff[flow.getSource()][flow.getDestination()][cos]++;
			this.blockedBandwidthPairs[flow.getSource()][flow.getDestination()] += flow.getRate();
			this.blockedBandwidthPairsDiff[flow.getSource()][flow.getDestination()][cos] += flow.getRate();
		}
	}
	
	public void departureFlow(int CoS) {
		this.departedFlowsDiff[CoS-1]++;
		this.departedFlows++;
	}

	/**
	 * Adds an event to the statistics.
	 * 
	 * @param event the Event object to be added
	 */
	public void addEvent(Event event) {
		simTime = event.getTime();
		try {
			if (event instanceof FlowArrivalEvent) {
				this.numberArrivals++;
				if (this.numberArrivals > this.minNumberArrivals) {
					int cos = ((FlowArrivalEvent) event).getFlow().getCOS()-1;
					this.arrivals++;
					this.arrivalsDiff[cos]++;
					this.requiredBandwidth += ((FlowArrivalEvent) event).getFlow().getRate();
					this.requiredBandwidthDiff[cos] += ((FlowArrivalEvent) event).getFlow().getRate();
					this.arrivalsPairs[((FlowArrivalEvent) event).getFlow().getSource()][((FlowArrivalEvent) event).getFlow().getDestination()]++;
					this.arrivalsPairsDiff[((FlowArrivalEvent) event).getFlow().getSource()][((FlowArrivalEvent) event).getFlow().getDestination()][cos]++;
					this.requiredBandwidthPairs[((FlowArrivalEvent) event).getFlow().getSource()][((FlowArrivalEvent) event).getFlow().getDestination()] += ((FlowArrivalEvent) event).getFlow().getRate();
					this.requiredBandwidthPairsDiff[((FlowArrivalEvent) event).getFlow().getSource()][((FlowArrivalEvent) event).getFlow().getDestination()][cos] += ((FlowArrivalEvent) event).getFlow().getRate();
				}
				if (Simulator.verbose && Math.IEEEremainder((double) arrivals, (double) 10000) == 0) {
					System.out.println(Integer.toString(arrivals));
				}
			}
			else if (event instanceof FlowDepartureEvent) {
				if (this.numberArrivals > this.minNumberArrivals) {
					this.departures++;
				}
				Flow f = ((FlowDepartureEvent)event).getFlow();
				if (f.isAccepeted()){
					this.numberOfUsedTransponders[f.getSource()][f.getDestination()]--;
				}
			}
			if (this.numberArrivals % 100 == 0){
				calculatePeriodicalStatistics();

			}
			if (this.numberArrivals % 5000 == 0){

				//            	System.out.println(event.getTime()+","+BFR);
			}
		}

		catch (Exception e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * This function is called during the simulation execution, but only if
	 * verbose was activated.
	 * 
	 * @return string with the obtained statistics
	 */
	public String fancyStatistics() {
		float acceptProb, blockProb, bbr;
		if (accepted == 0) {
			acceptProb = 0;
		} else {
			acceptProb = ((float) accepted) / ((float) arrivals) * 100;
		}
		if (blocked == 0) {
			blockProb = 0;
			bbr = 0;
		} else {
			blockProb = ((float) blocked) / ((float) arrivals) * 100;
			bbr = ((float) blockedBandwidth) / ((float) requiredBandwidth) * 100;
		}

		String stats = "Arrivals \t: " + Integer.toString(arrivals) + "\n";
		stats += "Required BW \t: " + Integer.toString(requiredBandwidth) + "\n";
		stats += "Departures \t: " + Integer.toString(departures) + "\n";
		stats += "Accepted \t: " + Integer.toString(accepted) + "\t(" + Float.toString(acceptProb) + "%)\n";
		stats += "Blocked \t: " + Integer.toString(blocked) + "\t(" + Float.toString(blockProb) + "%)\n";
		stats += "BBR     \t: " + Float.toString(bbr) + "%\n";
		stats += "\n";
		stats += "Blocking probability per s-d pair:\n";
		for (int i = 0; i < numNodes; i++) {
			for (int j = i + 1; j < numNodes; j++) {
				stats += "Pair (" + Integer.toString(i) + "->" + Integer.toString(j) + ") ";
				stats += "Calls (" + Integer.toString(arrivalsPairs[i][j]) + ")";
				if (blockedPairs[i][j] == 0) {
					blockProb = 0;
					bbr = 0;
				} else {
					blockProb = ((float) blockedPairs[i][j]) / ((float) arrivalsPairs[i][j]) * 100;
					bbr = ((float) blockedBandwidthPairs[i][j]) / ((float) requiredBandwidthPairs[i][j]) * 100;
				}
				stats += "\tBP (" + Float.toString(blockProb) + "%)";
				stats += "\tBBR (" + Float.toString(bbr) + "%)\n";
			}
		}

		return stats;
	}

	/**
	 * Prints all the obtained statistics, but only if verbose was not activated.
	 *
	 */
	public void printStatistics() {
		int count = 0;
		float bp, bbr, jfi, sum1 = 0, sum2 = 0;
		float bpDiff[], bbrDiff[];

		if (blocked == 0) {
			bp = 0;
			bbr = 0;
		} else {
			bp = ((float) blocked) / ((float) arrivals) * 100;
			bbr = ((float) blockedBandwidth) / ((float) requiredBandwidth) * 100;
		}
		bpDiff = new float[numClasses];
		bbrDiff = new float[numClasses];
		for (int i = 0; i < numClasses; i++) {
			if (blockedDiff[i] == 0) {
				bpDiff[i] = 0;
				bbrDiff[i] = 0;
			} else {
				bpDiff[i] = ((float) blockedDiff[i]) / ((float) arrivalsDiff[i]) * 100;
				bbrDiff[i] = ((float) blockedBandwidthDiff[i]) / ((float) requiredBandwidthDiff[i]) * 100;
			}
		}
		System.out.println("MBP " + Float.toString(bp));
		for (int i = 0; i < numClasses; i++) {
			System.out.println("MBP-" + Integer.toString(i) + " " + Float.toString(bpDiff[i]));
		}
		System.out.println("MBBR " + Float.toString(bbr));
		for (int i = 0; i < numClasses; i++) {
			System.out.println("MBBR-" + Integer.toString(i) + " " + Float.toString(bbrDiff[i]));
		}

		for (int i = 0; i < numNodes; i++) {
			for (int j = i + 1; j < numNodes; j++) {
				if (i != j) {
					System.out.print(Integer.toString(i) + "-" + Integer.toString(j) + " ");
					System.out.print("A " + Integer.toString(arrivalsPairs[i][j]) + " ");
					if (blockedPairs[i][j] == 0) {
						bp = 0;
						bbr = 0;
					} else {
						bp = ((float) blockedPairs[i][j]) / ((float) arrivalsPairs[i][j]) * 100;
						bbr = ((float) blockedBandwidthPairs[i][j]) / ((float) requiredBandwidthPairs[i][j]) * 100;
					}
					count++;
					sum1 += bbr;
					sum2 += bbr * bbr;
					System.out.print("BP " + Float.toString(bp) + " ");
					System.out.println("BBR " + Float.toString(bbr));
				}
			}
		}
		jfi = (sum1 * sum1) / ((float) count * sum2);
		System.out.println("JFI " + Float.toString(jfi));
		//Diff
		for (int c = 0; c < numClasses; c++) {
			count = 0;
			sum1 = 0;
			sum2 = 0;
			for (int i = 0; i < numNodes; i++) {
				for (int j = i + 1; j < numNodes; j++) {
					if (i != j) {
						if (blockedPairsDiff[i][j][c] == 0) {
							bp = 0;
							bbr = 0;
						} else {
							bp = ((float) blockedPairsDiff[i][j][c]) / ((float) arrivalsPairsDiff[i][j][c]) * 100;
							bbr = ((float) blockedBandwidthPairsDiff[i][j][c]) / ((float) requiredBandwidthPairsDiff[i][j][c]) * 100;
						}
						count++;
						sum1 += bbr;
						sum2 += bbr * bbr;
					}
				}
			}
			jfi = (sum1 * sum1) / ((float) count * sum2);
			System.out.println("JFI-" + Integer.toString(c) + " " + Float.toString(jfi));
		}
	}

	/**/public String[]  writeCSV(int seed, double forcedLoad) throws IOException{
	    	//Writer writer = Files.newBufferedWriter(Paths.get("block"+"-"+forcedLoad+seed+".csv"));
	    	//CSVWriter csvWriter = new CSVWriter(writer);
		
		//String [] sim = {String.valueOf(forcedLoad), String.valueOf(seed)};
	      
		celulas = 4* numClasses;
		String[] d  = new String[celulas++];
		d[0] = String.valueOf(forcedLoad);
		d[1] = String.valueOf(seed);
		d[2] = String.valueOf(this.blocked);
		
		for(int i = 3; i<=numClasses+2; i++) {
			d[i] =  String.valueOf(this.blockedDiff[i-3]);
		}
		 
		//if(numClasses != 1) {
			int k = 0;
			for (int j = numClasses +3;  j < 3*numClasses +2;  j+=2) {
				d[j] = String.valueOf(this.removedDiff[k]);
				d[j+1] =  String.valueOf(this.removedBackupDiff[k]);
				k++;
			}
		//}
		
		//linhas.add(sim);
		
		return d;
		//csvWriter.writeNext(c);
		//csvWriter.writeAll(linhas);

		//csvWriter.flush();
		//writer.close();
	    }/**/
	
	public void csvErrosLightPath(String rsaName, String topologyName, double forcedLoad, int seed) throws IOException {
		if(!errosLightPath.isEmpty()) {
			String[] first = {"Caminho", "ID", "Link", "Core", "Slot", "C-Ocupante"};
			Writer writer = Files.newBufferedWriter(Paths.get(rsaName + "-" + topologyName + "-errosLightPath-" + String.valueOf(forcedLoad) + "-" + String.valueOf(seed) + ".csv"));
	        CSVWriter csvWriter = new CSVWriter(writer);
	
	        csvWriter.writeNext(first);
	        csvWriter.writeAll(errosLightPath);
	
	        csvWriter.flush();
	        writer.close();
		}
	}

	/**
	 * Terminates the singleton object.
	 */
	public void finish()
	{
		singletonObject = null;
	}
}
