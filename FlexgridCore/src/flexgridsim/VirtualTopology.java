
package flexgridsim;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import org.w3c.dom.*;


/**
 * The virtual topology is created based on a given Physical Topology and
 * on the lightpaths specified on the XML file.
 * 
 * @author andred
 */
public class VirtualTopology {

	private long nextLightpathID = 0;
	private TreeSet<LightPath>[][] adjMatrix;
	private int adjMatrixSize;
	private Map<Long, LightPath> lightPaths;
	private PhysicalTopology pt;
	private Tracer tr = Tracer.getTracerObject();
	private MyStatistics st;
	//    private MyStatistics st = MyStatistics.getMyStatisticsObject();

	private static class LightPathSort implements Comparator<LightPath> {

		public int compare(LightPath lp1, LightPath lp2) {
			if (lp1.getID() < lp2.getID()) {
				return -1;
			}
			if (lp1.getID() > lp2.getID()) {
				return 1;
			}
			return 0;
		}
	}

	/**
	 * Creates a new VirtualTopology object.
	 * 
	 * @param xml file that contains all simulation information
	 * @param pt Physical Topology of the network
	 */
	@SuppressWarnings("unchecked")
	public VirtualTopology(Element xml, PhysicalTopology pt) {
		int nodes, lightpaths;

		lightPaths = new HashMap<Long, LightPath>();

		try {
			this.pt = pt;
			if (Simulator.verbose) {
				System.out.println(xml.getAttribute("name"));
			}

			adjMatrixSize = nodes = pt.getNumNodes();

			// Process lightpaths
			adjMatrix = new TreeSet[nodes][nodes];
			for (int i = 0; i < nodes; i++) {
				for (int j = 0; j < nodes; j++) {
					if (i != j) {
						adjMatrix[i][j] = new TreeSet<LightPath>(new LightPathSort());
					}
				}
			}
			NodeList lightpathlist = xml.getElementsByTagName("lightpath");
			lightpaths = lightpathlist.getLength();
			if (Simulator.verbose) {
				System.out.println(Integer.toString(lightpaths) + " lightpath(s)");
			}
			if (lightpaths > 0) {
				//TODO
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	/**
	 * First, creates a lightpath in the Physical Topology through the createLightpathInPT
	 * function. Then, gets the lightpath's source and destination nodes, so a new
	 * LightPath object can finally be created and added to the lightPaths HashMap
	 * and to the adjMatrix TreeSet.
	 *
	 * @param links list of integers that represent the links that form the lightpath
	 * @param channel list of channels to be allocated for each core
	 * @param modulationLevel the modulation level
	 * @return -1 if LightPath object cannot be created, or its unique identifier otherwise
	 */
	/**/public long createLightpath(Path path, int modulationLevel) {

		LightPath lp;
		int src, dst;
		long id;
		if (path.getLinks().length < 1) {
			throw (new IllegalArgumentException());
		} else {
			if (canCreateLightpath(path.getSlotList())) {

				createLightpathInPT(path.getSlotList());
				src = pt.getLink(path.getLinks()[0]).getSource();
				dst = pt.getLink(path.getLinks()[path.getLinks().length - 1]).getDestination();
				if(src==dst)
					dst =pt.getLink(path.getLinks()[path.getLinks().length - 2]).getDestination();
				id = this.nextLightpathID; 
				// System.out.println("ID: "+id);
				lp = new LightPath(id, src, dst, path, modulationLevel);
				adjMatrix[src][dst].add(lp);
				lightPaths.put(nextLightpathID, lp);
				tr.createLightpath(lp);
				this.nextLightpathID++;
				return id; 
			}
			//	System.out.println("ruim");ailS
			return -1;

		}
	}/**/

	public long createLightpath(Path path, int modulationLevel, int flowCOS, ControlPlane cp) {

		LightPath lp;
		int src, dst;
		long id;
		if (path.getLinks().length < 1) {
			throw (new IllegalArgumentException());
		} else {
			if (canCreateLightpath(path.getSlotList())) {
				createLightpathInPT(path.getSlotList());
				src = pt.getLink(path.getLinks()[0]).getSource();
				dst = pt.getLink(path.getLinks()[path.getLinks().length - 1]).getDestination();
				if(src==dst)
					dst =pt.getLink(path.getLinks()[path.getLinks().length - 2]).getDestination();
				id = this.nextLightpathID; 
				// System.out.println("ID: "+id);
				lp = new LightPath(id, src, dst, path, modulationLevel, flowCOS);
				adjMatrix[src][dst].add(lp);
				lightPaths.put(nextLightpathID, lp);
				tr.createLightpath(lp);
				this.nextLightpathID++;
				return id; 
			} else {
				//liberarLightPath(path, flowCOS, cp);
			}
			//	System.out.println("ruim");ailS
			return -1;

		}
	}

	public long createLightpath(Path path) {

		LightPath lp;
		int src, dst;
		long id;
		if (path.getLinks().length < 1) {
			throw (new IllegalArgumentException());
		} else {
			if (canCreateLightpath(path.getSlotList())) {

				createLightpathInPT(path.getSlotList());
				src = pt.getLink(path.getLinks()[0]).getSource();
				dst =pt.getLink(path.getLinks()[path.getLinks().length - 1]).getDestination();
				if(src==dst)
					dst =pt.getLink(path.getLinks()[path.getLinks().length - 2]).getDestination();
				id = this.nextLightpathID; 
				// System.out.println("ID: "+id);
				lp = new LightPath(id, src, dst, path);
				adjMatrix[src][dst].add(lp);
				lightPaths.put(nextLightpathID, lp);
				tr.createLightpath(lp);
				this.nextLightpathID++;
				return id; 
			}
			//	System.out.println("ruim");
			return -1;

		}
	}

	public long createLightpathProtection(Path pathBack, int modulationLevel) {
		long id;
		LightPath lp;
		int src, dst;
		if (pathBack.getLinks().length < 1||pathBack.getSlotList().isEmpty()) {
			throw (new IllegalArgumentException());

		} else {
			if (canCreateProtectionLightpath(pathBack.getSlotList())) {         
				createProtectionLightpathInPT(pathBack.getSlotList());
				src = pt.getLink(pathBack.getLinks()[0]).getSource();
				dst =pt.getLink(pathBack.getLinks()[pathBack.getLinks().length - 1]).getDestination();
				if(src==dst)
					dst =pt.getLink(pathBack.getLinks()[pathBack.getLinks().length -1]).getSource();
				id = this.nextLightpathID;
				lp = new LightPath(id, src, dst, pathBack, modulationLevel);
				adjMatrix[src][dst].add(lp);
				lightPaths.put(nextLightpathID, lp);
				tr.createLightpath(lp);
				this.nextLightpathID++;
				return id;
			}
			// System.out.println("VT - linha 142");
			return -1;
		}

	}

	public long createLightpathProtection(Path pathBack) {
		long id;
		LightPath lp;
		int src, dst;
		if (pathBack.getLinks().length < 1||pathBack.getSlotList().isEmpty()) {
			throw (new IllegalArgumentException());

		} else {
			if (canCreateProtectionLightpath(pathBack.getSlotList())) {         
				createProtectionLightpathInPT(pathBack.getSlotList());
				src = pt.getLink(pathBack.getLinks()[0]).getSource();
				dst =pt.getLink(pathBack.getLinks()[pathBack.getLinks().length - 1]).getDestination();
				if(src==dst)
					dst =pt.getLink(pathBack.getLinks()[pathBack.getLinks().length -1]).getSource();
				id = this.nextLightpathID;
				lp = new LightPath(id, src, dst, pathBack);
				adjMatrix[src][dst].add(lp);
				lightPaths.put(nextLightpathID, lp);
				tr.createLightpath(lp);
				this.nextLightpathID++;
				return id;
			}
			// System.out.println("VT - linha 142");
			return -1;
		}

	}

	public long getnextLightpathID(){
		return this.nextLightpathID;
	}


	/**
	 * First, removes a given lightpath in the Physical Topology through the removeLightpathInPT
	 * function. Then, gets the lightpath's source and destination nodes, to remove it 
	 * from the lightPaths HashMap and the adjMatrix TreeSet.
	 * 
	 * @param id the unique identifier of the lightpath to be removed
	 * @return true if operation was successful, or false otherwise
	 */
	public boolean removeLightPath(long id) {
		int src, dst;
		LightPath lp;

		if (id < 0) {
			throw (new IllegalArgumentException());
		} else {
			if (!lightPaths.containsKey(id)) {
				return false;
			}
			lp = lightPaths.get(id);
			removeLightpathFromPT( lp.getSlotList());
			src = lp.getSource();
			dst = lp.getDestination();
			lightPaths.remove(id);
			adjMatrix[src][dst].remove(lp);
			tr.removeLightpath(lp);

			return true;
		}
	}
	public boolean removeProtectionLightPath(long id) {
		int src, dst;
		LightPath lp;

		if (id < 0) {
			throw (new IllegalArgumentException());
		} else {
			if (!lightPaths.containsKey(id)) {
				return false;
			}
			lp = lightPaths.get(id);
			removeProtectionLightpathFromPT( lp.getSlotList());
			src = lp.getSource();
			dst = lp.getDestination();
			lightPaths.remove(id);
			adjMatrix[src][dst].remove(lp);
			tr.removeLightpath(lp);

			return true;
		}
	}
	
	public void slotsDegradation(LightPath oldLightpath, ArrayList<Slot> degradedSlots, LightPath newLightpath) {
		int src, dst;

		removeLightpathFromPT(degradedSlots);
		src = oldLightpath.getSource();
		dst = oldLightpath.getDestination();
		adjMatrix[src][dst].remove(oldLightpath);
		adjMatrix[src][dst].add(newLightpath);
		lightPaths.put(newLightpath.getID(), newLightpath);

		return;
	}
	
	public void undoSlotsDegradation(LightPath lightpath, ArrayList<Slot> recoveredSlots) {
		if (lightpath.getLinks().length < 1) {
			throw (new IllegalArgumentException());
		} else {
			if (canCreateLightpath(recoveredSlots)) {
				createLightpathInPT(recoveredSlots);
				adjMatrix[lightpath.getSource()][lightpath.getDestination()].add(lightpath);
				lightPaths.put(nextLightpathID, lightpath); 
			}
			//	System.out.println("ruim");
			return;
		}
	}

	/**
	 * Removes a given lightpath from the Physical Topology and then puts it back,
	 * but with a new route (set of links).
	 *
	 * @param id unique identifier of the lightpath to be rerouted
	 * @param links list of integers that represent the links that form the lightpath
	 * @param channel list of channels to be rerouted for each core
	 * @param modulationLevel the modulation level
	 * @return true if operation was successful, or false otherwise
	 */
	public boolean rerouteLightPath(long id, int[] links, Path path, int modulationLevel,  int modulationLevelBack) {
		int src, dst;
		LightPath old, lp;
		if (links.length < 1) {
			throw (new IllegalArgumentException());
		} else {
			if (!lightPaths.containsKey(id)) {
				return false;
			}
			old = lightPaths.get(id);
			removeLightpathFromPT( old.getSlotList());
			if (!canCreateLightpath( path.getSlotList())) {
				createLightpathInPT( old.getSlotList());
				return false;
			}
			createLightpathInPT( path.getSlotList());
			src = pt.getLink(links[0]).getSource();
			dst = pt.getLink(links[links.length - 1]).getDestination();
			adjMatrix[src][dst].remove(old);
			lp = new LightPath(id, src, dst, path, modulationLevel);
			adjMatrix[src][dst].add(lp);
			lightPaths.put(id, lp);
			return true;
		}
	}




	//    /**
	//     * Says whether or not a given lightpath is idle, i.e.,
	//     * all its bandwidth is available.
	//     * 
	//     * @param id the lightpath's unique identifier
	//     * @return true if lightpath is idle, or false otherwise
	//     */
	//    public boolean isLightpathIdle(long id) {
	//        int[] links;
	//        int firstSlot, lastSlot;
	//        links = getLightpath(id).getLinks();
	//        firstSlot = getLightpath(id).getFirstSlot();
	//        lastSlot  = getLightpath(id).getLastSlot();
	//        
	//        return pt.getLink(links[0]).areSlotsAvailable(firstSlot, lastSlot);
	//    }

	/**
	 * Retrieves a determined LightPath object from the Virtual Topology.
	 * 
	 * @param id the lightpath's unique identifier
	 * @return the required lightpath
	 */
	public LightPath getLightpath(long id) {
		if (id < 0) {
			throw (new IllegalArgumentException());
		} else {
			if (lightPaths.containsKey(id)) {
				return lightPaths.get(id);
			} else {
				return null;
			}
		}
	}

	/**
	 * Retrieves the TreeSet with all LightPath objects that
	 * belong to the Virtual Topology.
	 * 
	 * @param src the lightpath's source node
	 * @param dst the lightpath's destination node
	 * @return the TreeSet with all of the lightpaths
	 */
	public TreeSet<LightPath> getLightpaths(int src, int dst) {
		return new TreeSet<LightPath>(adjMatrix[src][dst]);
	}

	/**
	 * Retrieves the adjacency matrix of the Virtual Topology.
	 * 
	 * @return the VirtualTopology object's adjMatrix
	 */
	public TreeSet<LightPath>[][] getAdjMatrix() {
		return adjMatrix;
	}

	/**
	 * Says whether or not a lightpath exists, based only on its source
	 * and destination nodes.
	 * 
	 * @param src the lightpath's source node
	 * @param dst the lightpath's destination node
	 * @return true if the lightpath exists, or false otherwise
	 */
	public boolean hasLightpath(int src, int dst) {
		//System.out.println("hasLightpath"+Integer.toString(src)+" -> "+Integer.toString(dst));
		if (adjMatrix[src][dst] != null) {
			//System.out.println("Not null");
			if (!adjMatrix[src][dst].isEmpty()) {
				//System.out.println("Not empty");
				return true;
			}
		}
		return false;
	}

	/**
	 * Says whether or not a lightpath can be created, based only on its
	 * links and slots.
	 *
	 * @param links list of integers that represent the links that form the lightpath
	 * @param channel list of channels to be verified for each core
	 * @return true if the lightpath can be created, or false otherwise
	 */


	/**/public boolean canCreateLightpath( ArrayList<Slot> slots) {
		try {
			for (Slot slot: slots) {
				//if(slot.link == 12 && slot.core == 0 && slot.slot == 0)
					//System.out.println("Aqui");
				if (!pt.getLink(slot.link).isSlotsAvailable(slot)) {
					//System.out.println("VT LInha 427 - Ocupado: Link "+slot.link + " Core " + slot.core +" Slot "+ slot.slot);					
					if (fluxoQueAlocou(slot, slot.link) != -1) {
						//System.out.println("Primário: " + this.fluxoQueAlocou(slot, slot.link));
						st.setSlotOcupado(slot.link, slot.core, slot.slot, "Pri: " + String.valueOf(this.fluxoQueAlocou(slot, slot.link)));
					} else if (fluxoQueAlocouP(slot, slot.link) != -1) {
						//System.out.println("Backup: " + this.fluxoQueAlocouP(slot, slot.link));
						st.setSlotOcupado(slot.link, slot.core, slot.slot, "Back: " + String.valueOf(this.fluxoQueAlocouP(slot, slot.link)));
					} else {
						//System.out.println("ERRO!");
						st.setSlotOcupado(slot.link, slot.core, slot.slot, "ERRO");
					}
					return false;
				}
			}
		} catch (IllegalArgumentException e){
			System.out.println("Illegal argument for areSlotsAvailable");
			return false;
		}
		return true;
	}/**/

	public boolean liberarLightPath(Path path, int COS, ControlPlane cp) {
		boolean encontrado;

		try {
			for (Slot slot: path.getSlotList()) { 
				encontrado = false;
				if (!pt.getLink(slot.link).isSlotsAvailable(slot)) {
					long key;
					key = fluxoQueAlocou(slot, slot.link);
					encontrado = key != -1;
					if (encontrado) {
						if (pt.getLink(slot.link).getFlowsHere().get(key).getCOS() > COS) {
							if(cp.removeFlow(pt.getLink(slot.link).getFlowsHere().get(key).getID()) != null) {
								return true;
							}
						}
					}

					key = fluxoQueAlocouP(slot, slot.link);
					encontrado = key != -1;
					if (encontrado) {
						if (pt.getLink(slot.link).getFlowsHere().get(key).getCOS() > COS) {
							if(cp.removeFlow(pt.getLink(slot.link).getFlowsHere().get(key).getID()) != null) {
								return true;
							}
						}
					}

					/**for (long key : pt.getLink(slot.link).getpFlowsHere().keySet()) {
						//if (link.getFlowsHere().get(key))
						for (Slot slot1 : pt.getLink(slot.link).getpFlowsHere().get(key).getSlotList()) {
							if (slot.link == slot1.link && slot.core == slot1.core && slot.slot == slot1.slot) {
								cosenc = pt.getLink(slot.link).getFlowsHere().get(key).getCOS();
								if (pt.getLink(slot.link).getpFlowsHere().get(key).getCOS() > COS) {
								/**
					 * Talvez uma solução mais adequada aqui seria remover a alocação apenas do caminho de backup,
					 * caso o fluxo que o alocou tenha prioridade mais baixa, e não remover todo o fluxo.
					 *

 									if(cp.removeFlowProtection(pt.getLink(slot.link).getpFlowsHere().get(key).getID()) != null) {  
 										System.out.println("Caiu2P!");
 										return true;
									}
								}
								encontrado = true;
								break;
							}
						}
						if (encontrado) {
							break;
						}
					}/**/
					//System.out.println("VT LInha 486 - Ocupado: Link "+slot.link +" Slot "+ slot.slot + "   " + cosenc);
					return false;
				}
			}
		} catch (IllegalArgumentException e){
			System.out.println("Illegal argument for areSlotsAvailable");
			return false;
		}
		return true;
	}

	public boolean canCreateProtectionLightpath( ArrayList<Slot> slots) {
		try {
			for (Slot slot: slots) {
				//if(slot.link == 12 && slot.core == 0 && slot.slot == 0)
					//System.out.println("Aqui");
				if (!pt.getLink(slot.link).isProtectionSlotsAvailable(slot, pt.getOverlap())) {
					//System.out.println("VT LInha 501 - Ocupado: Link "+slot.link + " Core " + slot.core + " Slot "+ slot.slot);
					
					if (fluxoQueAlocou(slot, slot.link) != -1) {
						//System.out.println("Primário: " + this.fluxoQueAlocou(slot, slot.link));
						st.setSlotOcupado(slot.link, slot.core, slot.slot, "Pri: " + String.valueOf(this.fluxoQueAlocou(slot, slot.link)));
					} else if (fluxoQueAlocouP(slot, slot.link) != -1) {
						//System.out.println("Backup: " + this.fluxoQueAlocouP(slot, slot.link));
						st.setSlotOcupado(slot.link, slot.core, slot.slot, "Back: " + String.valueOf(this.fluxoQueAlocouP(slot, slot.link)));
					} else {
						//System.out.println("ERRO!");
						st.setSlotOcupado(slot.link, slot.core, slot.slot, "ERRO");
					}
					return false;	
				}
			}
		} catch (IllegalArgumentException e){
			System.out.println("Illegal argument for areSlotsAvailable");
			return false;
		}
		return true;
	}

	/*INCOMPLETO*/public boolean liberarLightPathProtection(Path path, int COS, ControlPlane cp) {
		try {
			for (Slot slot: path.getSlotList()) {
				if (!pt.getLink(slot.link).isProtectionSlotsAvailable(slot, pt.getOverlap())) {
					for (int link : path.getLinks()) {
						for (long key : pt.getLink(link).getFlowsHere().keySet()) {
							for (Slot slot1 : pt.getLink(link).getFlowsHere().get(key).getSlotList()) {
								if (slot == slot1) {
									if (pt.getLink(link).getFlowsHere().get(key).getCOS() > COS) {
										if(cp.removeFlow(pt.getLink(link).getFlowsHere().get(key).getID()) != null) {
											System.out.println("Caiu2!");
											return true;
										}
									}
								}
							}
						}
						for (long key : pt.getLink(link).getpFlowsHere().keySet()) {
							for (Slot slot1 : pt.getLink(link).getpFlowsHere().get(key).getSlotList()) {
								if (slot == slot1) {
									if (pt.getLink(link).getpFlowsHere().get(key).getCOS() > COS) {
										/**
										 * Talvez uma solução mais adequada aqui seria remover a alocação apenas do caminho de backup,
										 * caso o fluxo que o alocou tenha prioridade mais baixa, e não remover todo o fluxo.
										 */
										if(cp.removeFlowProtection(pt.getLink(link).getpFlowsHere().get(key).getID()) != null) {  
											System.out.println("Caiu2P!");
											return true;
										}
									}
								}
							}
						}
					}
					//System.out.println("VT LInha 546 - Ocupado: Link "+slot.link +" Slot "+ slot.slot);
					return false;
				}
			}
		} catch (IllegalArgumentException e){
			System.out.println("Illegal argument for areSlotsAvailable");
			return false;
		}
		return true;
	}/**/

	/**
	 * Reserves, in the physical topology, the resources a given lightpath needs:
	 * links, wavelengths and wavelength converters (if necessary).
	 * 
	 * @param links list of integers that represent the links that form the lightpath 
	 * @param firstSlot list of wavelength values used in the lightpath links
	 */
	private void createLightpathInPT( ArrayList<Slot> slots) {
		for (Slot slot: slots) {
			/**if(slot.link == 31 && slot.core == 0 && slot.slot == 43) {
				System.out.println("Alocou Pri");
			}/**/
			pt.getLink(slot.link).reserveSlot(slot);
		}
	}
	private void createProtectionLightpathInPT(ArrayList<Slot> slots) {
		for (Slot slot: slots) {
			/**if(slot.link == 31 && slot.core == 0 && slot.slot == 43) {
				System.out.println("Alocou Back");
			}/**/
			pt.getLink(slot.link).reserveProtectionSlot(slot,pt.getOverlap());
		}
	}
	/**
	 * Releases, in the physical topology, the resources a given lightpath was using:
	 * links, wavelengths and wavelength converters (if necessary).
	 * 
	 * @param links list of integers that represent the links that form the lightpath
	 * @param firstSlot list of wavelength values used in the lightpath links
	 */
	private void removeLightpathFromPT( ArrayList<Slot> slots) {
		for (Slot slot: slots) {
			pt.getLink(slot.link).releaseSlot(slot);
		}
	}

	private void removeProtectionLightpathFromPT( ArrayList<Slot> slots) {  

		for (Slot slot: slots) {
			pt.getLink(slot.link).releaseProtectionSlot(slot,pt.getOverlap());
		}
	}

	public long fluxoQueAlocou(Slot slot, int link) {
		for (long key : pt.getLink(link).getFlowsHere().keySet()) {
			for (Slot slot1 : pt.getLink(link).getFlowsHere().get(key).getSlotList()) {
				if (slot.link == slot1.link && slot.core == slot1.core && slot.slot == slot1.slot) {
					return key;
				}
			}
		}
		return -1;
	}

	public long fluxoQueAlocou(int link, int core, int slot) {
		for (long key : pt.getLink(link).getFlowsHere().keySet()) {
			for (Slot slot1 : pt.getLink(link).getFlowsHere().get(key).getSlotList()) {
				if (link == slot1.link && core == slot1.core && slot == slot1.slot) {
					return key;
				}
			}
		}
		return -1;
	}

	public long fluxoQueAlocouP(Slot slot, int link) {
		for (long key : pt.getLink(link).getpFlowsHere().keySet()) {
			for (Slot slot1 : pt.getLink(link).getpFlowsHere().get(key).getSlotListp()) {
				if (slot.link == slot1.link && slot.core == slot1.core && slot.slot == slot1.slot) {
					return key;
				}
			}
		}
		return -1;
	}

	public long fluxoQueAlocouP(int link, int core, int slot) {
		for (long key : pt.getLink(link).getpFlowsHere().keySet()) {
			for (Slot slot1 : pt.getLink(link).getpFlowsHere().get(key).getSlotListp()) {
				if (link == slot1.link && core == slot1.core && slot == slot1.slot) {
					return key;
				}
			}
		}
		return -1;
	}

	public void addSt(MyStatistics st) {
		this.st = st;
	}
	
	/**
	 * Prints all lightpaths belonging to the Virtual Topology.
	 * 
	 * @return string containing all the elements of the adjMatrix TreeSet
	 */
	@Override
	public String toString() {
		String vtopo = "";
		for (int i = 0; i < adjMatrixSize; i++) {
			for (int j = 0; j < adjMatrixSize; j++) {
				if (adjMatrix[i][j] != null) {
					if (!adjMatrix[i][j].isEmpty()) {
						vtopo += adjMatrix[i][j].toString() + "\n\n";
					}
				}
			}
		}
		return vtopo;
	}
	
}